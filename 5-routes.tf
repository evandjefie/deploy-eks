# private routing table
resource "aws_route_table" "eks_0_route_pv" {
  vpc_id = aws_vpc.eks_0_vpc.id

  route = [
    {
      cidr_block                 = "0.0.0.0/0"
      network_interface_id       = aws_nat_gateway.eks_0_nat.id
      nat_gateway_id             = ""
      gateway_id                 = ""
      carrier_gateway_id         = ""
      destination_prefix_list_id = ""
      egress_only_gateway_id     = ""
      instance_id                = ""
      ipv6_cidr_block            = ""
      local_gateway_id           = ""
      transit_gateway_id         = ""
      vpc_endpoint_id            = ""
      vpc_peering_connection_id  = ""
      core_network_arn           = ""
    },
  ]

  tags = {
    Name = "eks_0_route_pv"
  }
}

# public routing table
resource "aws_route_table" "eks_0_route_pb" {
  vpc_id = aws_vpc.eks_0_vpc.id

  route = [
    {
      cidr_block                 = "0.0.0.0/0"
      gateway_id                 = aws_internet_gateway.eks_0_gtw.id
      nat_gateway_id             = ""
      carrier_gateway_id         = ""
      destination_prefix_list_id = ""
      egress_only_gateway_id     = ""
      instance_id                = ""
      ipv6_cidr_block            = ""
      local_gateway_id           = ""
      network_interface_id       = ""
      transit_gateway_id         = ""
      vpc_endpoint_id            = ""
      vpc_peering_connection_id  = ""
      core_network_arn           = ""
    },
  ]

  tags = {
    Name = "eks_0_route_pb"
  }
}

# Add private subnets to private route table
resource "aws_route_table_association" "eks_0_route_pv_assoc_us_west_2b" {
  subnet_id      = aws_subnet.eks_0_sub_pv_us_west_2b.id
  route_table_id = aws_route_table.eks_0_route_pv.id
}

resource "aws_route_table_association" "eks_0_route_pv_assoc_us_west_2c" {
  subnet_id      = aws_subnet.eks_0_sub_pv_us_west_2c.id
  route_table_id = aws_route_table.eks_0_route_pv.id
}


# Add public subnets to public route table
resource "aws_route_table_association" "eks_0_route_pb_assoc_us_west_2b" {
  subnet_id      = aws_subnet.eks_0_sub_pb_us_west_2b.id
  route_table_id = aws_route_table.eks_0_route_pb.id
}

resource "aws_route_table_association" "eks_0_route_pb_assoc_us_west_2c" {
  subnet_id      = aws_subnet.eks_0_sub_pb_us_west_2c.id
  route_table_id = aws_route_table.eks_0_route_pb.id
}
