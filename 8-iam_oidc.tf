# Generate tls certificate
data "tls_certificate" "eks_0_tls_demo" {
  url = aws_eks_cluster.eks_0_cluster_demo.identity[0].oidc[0].issuer
}

# To manage permission for your apps deployed in k8s
# Generate oidc 
resource "aws_iam_openid_connect_provider" "eks_0_oidc_demo" {
  client_id_list  = ["sts.amazonaws.com"]
  thumbprint_list = [data.tls_certificate.eks_0_tls_demo.certificates[0].sha1_fingerprint]
  url             = aws_eks_cluster.eks_0_cluster_demo.identity[0].oidc[0].issuer
}