# Used in private subnet to allow services to connect to internet

# Allow to have ip adress
resource "aws_eip" "eks_0_nat_eip" {
  vpc = true

  tags = {
    Name = "eks_0_nat_eip"
  }
}

resource "aws_nat_gateway" "eks_0_nat" {
  allocation_id = aws_eip.eks_0_nat_eip.id
  subnet_id     = aws_subnet.eks_0_sub_pb_us_west_2b.id

  tags = {
    Name = "eks_0_nat"
  }

  depends_on = [
    aws_internet_gateway.eks_0_gtw
  ]
}