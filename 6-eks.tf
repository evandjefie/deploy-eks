# Add role for use cluster
resource "aws_iam_role" "eks_0_role_cluster_demo" {
  name = "eks_0_role_cluster_demo"

  assume_role_policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": {
                "Service": "eks.amazonaws.com"
            },
            "Action": "sts:AssumeRole"
        }
    ]
}  
POLICY
}

# Attach this policy to the role
resource "aws_iam_role_policy_attachment" "demo_AmazonEKSClusterPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.eks_0_role_cluster_demo.name
}

# Cluster settings
resource "aws_eks_cluster" "eks_0_cluster_demo" {
  name     = "demo"
  role_arn = aws_iam_role.eks_0_role_cluster_demo.arn

  vpc_config {
    subnet_ids = [
      aws_subnet.eks_0_sub_pv_us_west_2b.id,
      aws_subnet.eks_0_sub_pv_us_west_2c.id,
      aws_subnet.eks_0_sub_pb_us_west_2b.id,
      aws_subnet.eks_0_sub_pb_us_west_2b.id,
    ]
  }

  depends_on = [
    aws_iam_role_policy_attachment.demo_AmazonEKSClusterPolicy
  ]

}