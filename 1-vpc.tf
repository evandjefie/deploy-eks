resource "aws_vpc" "eks_0_vpc" {
  cidr_block = "10.0.0.0/16" # This block indicate the size of network (~65000 ip) | 255.255.0.0

  tags = {
    Name = "eks_0_vpc"
  }
}