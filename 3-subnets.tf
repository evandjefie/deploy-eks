# Eks requiert to have 2 publics subnets and 2 privates subnets on differents available zones

# PRIVATE SUBNETS
resource "aws_subnet" "eks_0_sub_pv_us_west_2b" {
  vpc_id            = aws_vpc.eks_0_vpc.id
  cidr_block        = "10.0.0.0/19" # This block indicate the size of network (~8000 ip) | 255.255.224.0
  availability_zone = "us-west-2b"

  tags = {
    # Eks require tag for subnet config
    "Name"                            = "eks_0_sub_pv_us_west_2b"
    "kubernetes.io/role/internal-elb" = "1" # to show where private lb will be create
    # cluster name is demo; "owned" option means demo is used only for k8s. Use "shared" option to share it to another service or k8 cluster
    "kubernetes.io/cluster/demo" = "owned"
  }

}

resource "aws_subnet" "eks_0_sub_pv_us_west_2c" {
  vpc_id            = aws_vpc.eks_0_vpc.id
  cidr_block        = "10.0.32.0/19" # This block indicate the size of network (~8000 ip) | 255.255.224.0
  availability_zone = "us-west-2c"

  tags = {
    # Eks require tag for subnet config
    "Name"                            = "eks_0_sub_pv_us_west_2c"
    "kubernetes.io/role/internal-elb" = "1"
    "kubernetes.io/cluster/demo"      = "owned"
  }

}

# PUBLIC SUBNETS
resource "aws_subnet" "eks_0_sub_pb_us_west_2b" {
  vpc_id                  = aws_vpc.eks_0_vpc.id
  cidr_block              = "10.0.64.0/19" # This block indicate the size of network (~8000 ip) | 255.255.224.0
  availability_zone       = "us-west-2b"
  map_public_ip_on_launch = true # for create public k8s instance groups

  tags = {
    # Eks require tag for subnet config
    "Name"                       = "eks_0_sub_pb_us_west_2b"
    "kubernetes.io/role/elb"     = "1" # this create public lb for in this subnet
    "kubernetes.io/cluster/demo" = "owned"
  }
}

resource "aws_subnet" "eks_0_sub_pb_us_west_2c" {
  vpc_id                  = aws_vpc.eks_0_vpc.id
  cidr_block              = "10.0.96.0/19" # This block indicate the size of network (~8000 ip) | 255.255.224.0
  availability_zone       = "us-west-2c"
  map_public_ip_on_launch = true

  tags = {
    # Eks require tag for subnet config
    "Name"                       = "eks_0_sub_pb_us_west_2c"
    "kubernetes.io/role/elb"     = "1"
    "kubernetes.io/cluster/demo" = "owned"
  }
}
# By passing public lb, all subnets become technically private