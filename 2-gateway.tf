#   Routing to ip public
# eks_0_gtw is the door, eks_0_vpc is the home
resource "aws_internet_gateway" "eks_0_gtw" {
  vpc_id = aws_vpc.eks_0_vpc.id

  tags = {
    Name = "eks_0_gtw"
  }
}