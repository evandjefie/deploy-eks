# Add role for nodes eks
resource "aws_iam_role" "eks_0_role_nodes_group_demo" {
  name = "eks_0_role_nodes_group_demo"

  assume_role_policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": {
                "Service": "ec2.amazonaws.com"
            },
            "Action": "sts:AssumeRole"
        }
    ]
}  
POLICY

  # assume_role_policy = jsonencode(
  #     {
  #         Statement =[{
  #             Action = ""
  #             Effect =""
  #             Principal = {
  #                 Service = "ec2.amazonaws.com"
  #             }
  #         }]
  #         Version = "2012-10-17"
  #     }
  # )
}

# Attach this policy to the role
resource "aws_iam_role_policy_attachment" "demo_AmazonEKSWorkerNodePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = aws_iam_role.eks_0_role_nodes_group_demo.name
}

resource "aws_iam_role_policy_attachment" "demo_AmazonEKS_CNI_Policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = aws_iam_role.eks_0_role_nodes_group_demo.name
}

# ECR repository
resource "aws_iam_role_policy_attachment" "demo_AmazonEC2ContainerRegistryReadOnly" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = aws_iam_role.eks_0_role_nodes_group_demo.name
}

# Setting eks nodes group
resource "aws_eks_node_group" "eks_0_nodes_group_pv_demo" {
  cluster_name    = aws_eks_cluster.eks_0_cluster_demo.name # link nodes group to cluster
  node_group_name = "eks_0_nodes_group_pv_demo"
  node_role_arn   = aws_iam_role.eks_0_role_nodes_group_demo.arn

  # subnets where this node group live
  subnet_ids = [
    aws_subnet.eks_0_sub_pv_us_west_2b.id,
    aws_subnet.eks_0_sub_pv_us_west_2c.id
  ]
  # change to public subnet if you need nodes with public IP

  capacity_type  = "ON_DEMAND" # "SPOT" option is much cheaper
  instance_types = ["t2.micro"]

  # eks will not autoscale nodes by itself
  # you need to add "cluster autoscaller" component. It adjust desired_size according request workload
  scaling_config {
    desired_size = 1
    max_size     = 2
    min_size     = 0
  }

  update_config {
    max_unavailable = 1 # max number of nodes unavailable during an update      
  }

  # Useful to identify a specific node group
  labels = {
    role = "general"
  }

  # an alternative to identify a specific node group
  # taint {
  #     key = "team"
  #     value = "devops"
  #     effect = "NO_SCHEDULE"
  # }

  # create a custom config for k8s node
  # launch_template {
  #     name = aws_launch_template.eks-with-disks.name
  #     version = aws_launch_template.eks-with-disks.latest_version
  # }

  depends_on = [
    aws_iam_role_policy_attachment.demo_AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.demo_AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.demo_AmazonEC2ContainerRegistryReadOnly
  ]

  # for ex add disk to workers node
  # resource "aws_launch_template" "eks_with_disks" {
  # name = "eks_with_disks"

  # block_device_mappings {
  #   device_name = "/dev/xvdb"
  #   # device_name = "/dev/sda1"

  #   ebs {
  #     volume_size = 20
  #     volume_type = "gp2"
  #   }
  # }

}